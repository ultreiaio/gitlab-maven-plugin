# GitLab Maven plugin

 * Author [Tony Chemit](mailto:dev@tchemit.fr)
 * Last generated at 2021-12-16 17:03.

## Version [1.0.20](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/22)

**Closed at 2021-12-16.**


### Issues
  * [[Bug 48]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/48) **Fix Artifact classifier (place before version)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 47]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/47) **Upgrade to java 11** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 49]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/49) **Add forceArtifacts parameter to force download artifacts** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 50]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/50) **Let&#39;s build the cache using milestone title instead of id, We are not robots!** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 51]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/51) **Add extra artifacts directory and remove staging logic** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 52]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/52) **Introduce download-extra-artifacts mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.19](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/21)

**Closed at 2021-09-12.**


### Issues
  * [[Bug 46]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/46) **Fix maven dependencies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.18](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/20)

**Closed at 2021-09-12.**


### Issues
  * [[Bug 43]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/43) **Artifacts are not anymore detected from maven** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Bug 44]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/44) **Artifacts are not anymore detected if artifacts file exists and is an empty json array** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 45]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/45) **Use maven 3.8.2 API** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.17](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/19)

**Closed at 2020-08-05.**


### Issues
  * [[Bug 42]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/42) **Bad date pattern in some model** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 40]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/40) **Links on generated CHANGELOG are wrong** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 41]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/41) **Improve generated links** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.16](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/18)

**Closed at 2020-01-31.**


### Issues
  * [[Bug 39]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/39) **Fix reopen milestone** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.15](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/17)

**Closed at 2019-03-10.**


### Issues
  * [[Feature 37]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/37) **Add Reopen milestone mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 38]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/38) **Add forceAddDueDate to close milestone Mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.14](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/16)

**Closed at 2019-02-13.**


### Issues
  * [[Feature 35]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/35) **When creating a milestone, let&#39;s set his start date** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 36]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/36) **When closing a milestone let&#39;s set his due date if not filled** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.13](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/15)

**Closed at 2018-05-18.**


### Issues
  * [[Feature 33]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/33) **Update dependencies** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 34]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/34) **Introduce a cache for milestones artifacts.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.12](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/14)

**Closed at *In progress*.**


### Issues
  * [[Bug 31]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/31) **Author are not rendered on generated files.** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 32]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/32) **Add a flatLayout on ReleaseArtifactModel to manage with none maven repository for staging** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.11](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/13)

**Closed at *In progress*.**


### Issues
  * [[Bug 29]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/29) **If you don&#39;t fill fromMilestone you got no milestone at all** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.10](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/12)

**Closed at *In progress*.**


### Issues
  * [[Feature 27]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/27) **Add a *fromMilestone* parameter to restrict the list of milestone to use** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 28]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/28) **By default plugin should not be quiet** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 26]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/26) **Update libraries** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.9](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/11)

**Closed at *In progress*.**


### Issues
  * [[Feature 24]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/24) **Add a new flag *addDueToDate* to fill dueToDate when closing milestone** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 25]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/25) **Add a new mojo to compute milestone estimate time** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.8](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/10)

**Closed at *In progress*.**


### Issues
  * [[Feature 20]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/20) **up to pom 5** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 21]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/21) **update to pom 7** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 22]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/22) **migrate project to gitlab.com/ultreiaio/gitlab-maven-plugin** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 23]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/23) **add site (see https://ultreiaio.gitlab.io/gitla-maven-plugin)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.7](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/9)

**Closed at 2017-03-12.**


### Issues
  * [[Bug 19]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/19) **Sort milestones using version natural order** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 15]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/15) **Add a close-milestone mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 16]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/16) **Add a check-milestone-exist** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 17]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/17) **Add create milestone mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Task 18]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/18) **Update pom to version 4** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.6](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/8)

**Closed at *In progress*.**


### Issues
  * [[Bug 14]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/14) **Bad milestone usage in generate-staging-changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.5](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/7)

**Closed at *In progress*.**


### Issues
  * [[Feature 13]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/13) **Add generate-staging-changelog mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.4](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/6)

**Closed at 2017-02-21.**


### Issues
  * [[Feature 11]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/11) **Improve trackers usage in generate mojo configuration** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 12]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/12) **Add staging mode for changelog generation** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.3](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/5)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 8]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/8) **Add deployed artifacts for each release into generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 9]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/9) **show only closed milestones in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 10]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/10) **Improve issues display in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.2](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/4)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 5]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/5) **Fix changelog template (missing a line after each milestone)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 6]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/6) **Add milestone link in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 7]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/7) **Improve date format in generated changelog** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.1](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/3)

**Closed at 2017-02-20.**


### Issues
  * [[Bug 4]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/4) **Fix changelog template (missing a line after each tracker)** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

## Version [1.0.0](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/milestones/1)

**Closed at 2017-02-20.**


### Issues
  * [[Feature 1]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/1) **Add download-milestones mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 2]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/2) **Add generate-changes mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)
  * [[Feature 3]](https://gitlab.com/ultreiaio/gitlab-maven-plugin/-/issues/3) **Add generate-changelog mojo** (Thanks to Tony CHEMIT) (Reported by Tony CHEMIT)

