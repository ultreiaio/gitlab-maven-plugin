package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import io.ultreia.java4all.lang.Strings;
import io.ultreia.maven.gitlab.model.MilestoneReleaseArtifactModel;
import io.ultreia.maven.gitlab.model.ReleaseArtifactModel;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.settings.Server;
import org.apache.maven.settings.Settings;
import org.gitlab.api.GitlabAPI;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcher;
import org.sonatype.plexus.components.sec.dispatcher.SecDispatcherException;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public abstract class GitlabMojoSupport extends AbstractMojo {

    @Parameter(property = "gitlab.host", required = true)
    private String host;

    @Parameter(property = "gitlab.organizationId", required = true)
    private String organizationId;

    @Parameter(property = "gitlab.projectId", required = true)
    private String projectId;

    @Parameter(property = "gitlab.serverId", required = true)
    private String serverId;

    @Parameter(property = "gitlab.cachePath", required = true, defaultValue = "${project.build.directory}/gitlab-cache")
    private File cachePath;

    /**
     * To add extra definition in cache (could be milestones, or artifacts).
     * <p>
     * Will be added to the gitlab cache.
     */
    @Parameter(property = "gitlab.cacheExtraPath", required = true, defaultValue = "src/gitlab")
    private File cacheExtraPath;

    @Parameter(property = "gitlab.quiet", defaultValue = "false")
    private boolean quiet;

    /**
     * This will cause the execution to be run only at the top of a given module tree. That is, run in the project
     * contained in the same folder where the mvn execution was launched.
     */
    @Parameter(property = "gitlab.runOnlyAtExecutionRoot", defaultValue = "true")
    private boolean runOnlyAtExecutionRoot;

    /**
     * The current project base directory.
     */
    @Parameter(property = "basedir", required = true)
    private String basedir;

    @Parameter(defaultValue = "${settings}", readonly = true)
    private Settings settings;

    /**
     * The Maven Session.
     */
    @Parameter(defaultValue = "${session}", readonly = true, required = true)
    private MavenSession mavenSession;

    /**
     * password decipher
     */
    @Component(hint = "gitlab-maven-plugin")
    private SecDispatcher sec;

    abstract boolean isSkip();

    abstract void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException, ProjectNotFoundException;

    public boolean isVerbose() {
        return !quiet;
    }

    String getGitlabProjectPath() {
        return organizationId + "/" + projectId;
    }

    @Override
    public final void execute() throws MojoExecutionException, MojoFailureException {

        if (isSkip()) {
            getLog().info("Skip goal.");
            return;
        }

        if (isVerbose()) {
            getLog().info("Connect to " + host);
        }
        GitlabAPIExt api = initApi();
        try {
            execute(api);
        } catch (IOException e) {
            throw new MojoExecutionException("Error with api", e);
        } catch (MilestoneNotFoundException e) {
            throw new MojoExecutionException("Could not find milestone", e);
        } catch (ProjectNotFoundException e) {
            throw new MojoExecutionException("Could not find project", e);
        }
    }

    public GitlabCache newCache() {
        GitlabCache cache = new GitlabCache(cachePath.toPath(), cacheExtraPath.toPath());
        if (isVerbose()) {
            getLog().info(String.format("Use cache at: %s", cachePath));
        }
        return cache;
    }

    public String getHost() {
        return host;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public String getProjectId() {
        return projectId;
    }

    boolean shouldSkip() {
        // Run only at the execution root
        if (runOnlyAtExecutionRoot && !isThisTheExecutionRoot()) {
            getLog().info("Skipping the Changes Report in this project because it's not the Execution Root");
            return true;
        }
        return false;
    }

    Mustache getMustache(String templateName) {
        MustacheFactory mf = new DefaultMustacheFactory("templates");
        return mf.compile(templateName);
    }

    protected String removeSnapShot(String milestone) {
        if (milestone.endsWith("-SNAPSHOT")) {
            milestone = Strings.removeEnd(milestone, "-SNAPSHOT");
        }
        return milestone;
    }

    protected Set<MilestoneReleaseArtifactModel> loadArtifacts(List<ReleaseArtifactModel> releaseArtifacts, String version) {
        Set<MilestoneReleaseArtifactModel> milestoneArtifacts = new LinkedHashSet<>();
        String host = "https://repo1.maven.org/maven2";
        for (ReleaseArtifactModel releaseArtifact : releaseArtifacts) {
            URL url = releaseArtifact.getUrl(host, version, false);
            try {
                InputStream urlConnection = url.openStream();
                getLog().info(String.format("Add release artifact: %s", url));
                urlConnection.close();
                milestoneArtifacts.add(new MilestoneReleaseArtifactModel(releaseArtifact.getName(), url));
            } catch (Exception e) {
                // nothing special to do
                getLog().debug("Could not get " + url);
            }
        }
        return milestoneArtifacts;
    }

    private GitlabAPIExt initApi() throws MojoExecutionException {

        Server server = settings.getServer(serverId);

        String encryptedPrivateKey = server.getPrivateKey();
        try {
            String privateKey = sec.decrypt(encryptedPrivateKey);
            return new GitlabAPIExt(GitlabAPI.connect(host, privateKey));
        } catch (SecDispatcherException e) {
            throw new MojoExecutionException("Could not decrypt privateKey", e);
        }
    }

    private boolean isThisTheExecutionRoot() {
        getLog().debug("Root Folder:" + mavenSession.getExecutionRootDirectory());
        getLog().debug("Current Folder:" + basedir);
        boolean result = mavenSession.getExecutionRootDirectory().equalsIgnoreCase(basedir);
        if (result) {
            getLog().debug("This is the execution root.");
        } else {
            getLog().debug("This is NOT the execution root.");
        }
        return result;
    }
}
