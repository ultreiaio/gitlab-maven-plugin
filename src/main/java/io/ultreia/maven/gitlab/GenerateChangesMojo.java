package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.Mustache;
import io.ultreia.maven.gitlab.model.ProjectModel;
import io.ultreia.maven.gitlab.model.ProjectModelBuilder;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;

import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.util.Arrays;

/**
 * Generate a {@code changes.xml} file with releases data used by {@code maven-changes-plugin} to generate project
 * releases history report.
 * <p>
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "generate-changes", defaultPhase = LifecyclePhase.PRE_SITE)
@Execute(goal = "download-milestones")
public class GenerateChangesMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    @Parameter(property = "gitlab.changesFile", required = true, defaultValue = "${project.basedir}/src/changes/changes.xml")
    private File changesFile;

    @Parameter(property = "gitlab.changesTitle", required = true)
    private String changesTitle;

    @Parameter(property = "gitlab.changesAuthor", required = true)
    private String changesAuthor;

    @Parameter(property = "gitlab.changesAuthorEmail", required = true)
    private String changesAuthorEmail;

    /**
     * Comma separated tracker names (they are gitlab label) and they match the type of issues used by the changes.xml
     * format {@code fix, add, update, remove} in this precise order.
     * <p>
     * <strong>Note:</strong> order matter
     */
    @Parameter(property = "gitlab.trackers", required = true)
    private String trackers;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    protected void execute(GitlabAPIExt api) throws IOException, ProjectNotFoundException {

        Files.createDirectories(changesFile.getParentFile().toPath());

        ProjectModel model = ProjectModelBuilder.create(this)
                .setAuthor(changesAuthor)
                .setAuthorEmail(changesAuthorEmail)
                .setTitle(changesTitle)
                .setTrackers(Arrays.asList(trackers.split("\\s*,\\s*")))
                .setMilestone(milestone)
                .build();

        Mustache mustache = getMustache("changes.xml.mustache");

        try (Writer writer = Files.newBufferedWriter(changesFile.toPath(), StandardCharsets.UTF_8)) {
            mustache.execute(writer, model).flush();
        }

        getLog().info(String.format("changes.xml generated to %s", changesFile.getParent()));

    }

}
