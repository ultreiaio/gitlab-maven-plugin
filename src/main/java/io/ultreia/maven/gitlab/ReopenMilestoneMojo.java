package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

/**
 * Created on 10/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since ?
 */

import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Get the given milestone and reopen it. Will fail if the milestone does not exist.
 * <p>
 * Created on 10/03/19.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "reopen-milestone", defaultPhase = LifecyclePhase.INITIALIZE)
public class ReopenMilestoneMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    /**
     * The first milestone to use.
     */
    @Parameter(property = "gitlab.fromMilestone")
    private String fromMilestone;

    /**
     * Should we remove due date to milestone to reopen?
     */
    @Parameter(property = "gitlab.removeDueDate", defaultValue = "true")
    private boolean removeDueDate;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException {
milestone = removeSnapShot(milestone);
        if (isVerbose()) {
            getLog().info("loading project...");
        }
        String projectPath = getGitlabProjectPath();
        GitlabProject gitlabProject = api.getProject(projectPath);

        GitlabCache gitlabCache = newCache();
        gitlabCache.setProject(gitlabProject);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        if (isVerbose()) {
            getLog().info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = api.getMilestones(gitlabProject, fromMilestone);
        getLog().info(String.format("found %d milestone(s).", gitlabMilestones.size()));

        Optional<GitlabMilestoneExt> optionalMilestone = gitlabMilestones.stream().filter(m -> milestone.equals(m.getTitle())).findFirst();
        if (!optionalMilestone.isPresent()) {
            throw new MilestoneNotFoundException(milestone);
        }

        GitlabMilestoneExt milestone = optionalMilestone.get();
        getLog().info(String.format("Reopen milestone %s.", milestone.getTitle()));

        api.reopenMilestone(milestone);
        if (removeDueDate && milestone.getDueDate() != null) {
            api.clearMilestoneDueDate(milestone);
        }

    }
}

