package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import io.ultreia.maven.gitlab.model.MilestoneReleaseArtifactModel;
import io.ultreia.maven.gitlab.model.ReleaseArtifactModel;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * To download in cache extra milestones artifacts.
 * <p>
 * Created on 16/12/2021.
 *
 * @author Tony Chemit - dev@tchemit.fr
 * @since 1.0.20
 */
@Mojo(name = "download-extra-artifacts", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
@Execute(goal = "download-milestones")
public class DownloadExtraArtifactsMojo extends GitlabMojoSupport {

    /**
     * Milestone to use to store artifacts.
     */
    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    /**
     * Release artifacts for each version.
     */
    @Parameter(required = true)
    private List<ReleaseArtifactModel> releaseArtifacts;
    /**
     * List of versions, separated by comma.
     * <p>
     * For each version, will try to add to {@code milestone} artifacts.
     */
    @Parameter(property = "gitlab.versions")
    private String versions;
    /**
     * Skip this mojo execution.
     */
    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    protected void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException {
        milestone = removeSnapShot(milestone);
        String projectPath = getGitlabProjectPath();

        GitlabCache gitlabCache = newCache();

        GitlabProject gitlabProject = gitlabCache.getProject().orElseThrow(RuntimeException::new);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        GitlabMilestoneExt milestone = gitlabCache.getMilestone(this.milestone).orElseThrow(RuntimeException::new);
        getLog().info(String.format("GitLab milestone (%s) id: %d", milestone.getTitle(), milestone.getId()));

        List<Version> versionsList = Arrays.stream(versions.split("\\s*,\\s*")).map(Version::valueOf).sorted().collect(Collectors.toList());
        Collections.reverse(versionsList);
        Set<MilestoneReleaseArtifactModel> milestoneArtifacts = new LinkedHashSet<>();
        for (Version version : versionsList) {
            Set<MilestoneReleaseArtifactModel> artifactsForVersion = loadArtifacts(releaseArtifacts, version.getVersion());
            if (artifactsForVersion != null) {
                milestoneArtifacts.addAll(artifactsForVersion);
            }
        }
        getLog().info(String.format("found %d artifact(s).", milestoneArtifacts.size()));
        gitlabCache.setMilestoneExtraArtifacts(milestone, milestoneArtifacts);
    }
}

