package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import io.ultreia.java4all.util.Version;
import io.ultreia.maven.gitlab.model.MilestoneReleaseArtifactModel;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabMilestone;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.lang.reflect.Type;
import java.nio.file.FileVisitResult;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.SimpleFileVisitor;
import java.nio.file.attribute.BasicFileAttributes;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GitlabCache {

    private final Path cachePath;
    private final Path cacheExtraPath;
    private final Gson gson;

    GitlabCache(Path cachePath, Path cacheExtraPath) {
        this.cachePath = cachePath;
        this.cacheExtraPath = cacheExtraPath;
        this.gson = new GsonBuilder().setPrettyPrinting().create();
    }

    public Optional<GitlabProject> getProject() throws IOException {
        Path projectPath = getProjectPath();
        if (Files.exists(projectPath)) {
            try (Reader reader = Files.newBufferedReader(projectPath)) {
                GitlabProject gitlabMilestone = gson.fromJson(reader, GitlabProject.class);
                return Optional.of(gitlabMilestone);
            }
        }
        return Optional.empty();
    }

    public void setProject(GitlabProject gitlabProject) throws IOException {
        Path projectPath = getProjectPath();
        Files.createDirectories(projectPath.getParent());
        try (Writer reader = Files.newBufferedWriter(projectPath)) {
            gson.toJson(gitlabProject, reader);
        }
    }

    public List<GitlabMilestoneExt> getMilestones() throws IOException {
        Path milestonesPath = cachePath.resolve("milestones");
        Set<Path> paths = getPaths(milestonesPath);
        LinkedList<GitlabMilestoneExt> result = new LinkedList<>();
        for (Path path : paths) {
            if (path.toFile().getName().endsWith("-issues.json")) {
                continue;
            }
            if (path.toFile().getName().endsWith("-artifacts.json")) {
                continue;
            }
            try (Reader reader = Files.newBufferedReader(path)) {
                GitlabMilestoneExt gitlabMilestone = gson.fromJson(reader, GitlabMilestoneExt.class);
                result.add(gitlabMilestone);
            }
        }
        result.sort(Comparator.comparing(m -> Version.valueOf(m.getTitle())));
        return result;
    }

    public Optional<GitlabMilestoneExt> getMilestone(String gitlabMilestone) throws IOException {
        Path milestonePath = getMilestonePath(gitlabMilestone);
        if (Files.exists(milestonePath)) {
            try (Reader reader = Files.newBufferedReader(milestonePath)) {
                return Optional.of(gson.fromJson(reader, GitlabMilestoneExt.class));
            }
        }
        return Optional.empty();
    }

    public Optional<GitlabMilestoneExt> getMilestone(GitlabMilestone gitlabMilestone) throws IOException {
        Path milestonePath = getMilestonePath(gitlabMilestone);
        if (Files.exists(milestonePath)) {
            try (Reader reader = Files.newBufferedReader(milestonePath)) {
                return Optional.of(gson.fromJson(reader, GitlabMilestoneExt.class));
            }
        }
        return Optional.empty();
    }

    public void setMilestone(GitlabMilestone gitlabMilestone) throws IOException {
        Path milestonePath = getMilestonePath(gitlabMilestone);
        Files.createDirectories(milestonePath.getParent());
        try (Writer reader = Files.newBufferedWriter(milestonePath)) {
            gson.toJson(gitlabMilestone, reader);
        }
    }

    public Set<GitlabIssue> getMilestoneIssues(GitlabMilestone gitlabMilestone) throws IOException {
        Path issuesPath = getIssuesPath(gitlabMilestone);
        try (Reader reader = Files.newBufferedReader(issuesPath)) {
            Type typeOfT = new TypeToken<Set<GitlabIssue>>() {
            }.getType();
            return gson.fromJson(reader, typeOfT);
        }
    }

    public void setMilestoneIssues(GitlabMilestone gitlabMilestone, Iterable<GitlabIssue> gitlabIssues) throws IOException {
        Path issuesPath = getIssuesPath(gitlabMilestone);
        Files.createDirectories(issuesPath.getParent());
        try (Writer reader = Files.newBufferedWriter(issuesPath)) {
            gson.toJson(gitlabIssues, reader);
        }
    }

    public Set<MilestoneReleaseArtifactModel> getMilestoneArtifacts(GitlabMilestone gitlabMilestone) throws IOException {
        Set<MilestoneReleaseArtifactModel> result = new LinkedHashSet<>();
        Path artifactsPath = getArtifactsPath(gitlabMilestone);
        if (Files.exists(artifactsPath)) {
            result.addAll(getMilestoneArtifacts(artifactsPath));
        }
        artifactsPath = getArtifactsExtraPath(gitlabMilestone);
        if (Files.exists(artifactsPath)) {
            result.addAll(getMilestoneArtifacts(artifactsPath));
        }
        return result.isEmpty() ? null : result;
    }

    Set<MilestoneReleaseArtifactModel> getMilestoneArtifacts(Path artifactsPath) throws IOException {
        if (Files.notExists(artifactsPath)) {
            return null;
        }
        try (Reader reader = Files.newBufferedReader(artifactsPath)) {
            Type typeOfT = new TypeToken<Set<MilestoneReleaseArtifactModel>>() {
            }.getType();
            return gson.fromJson(reader, typeOfT);
        }
    }

    public void setMilestoneArtifacts(GitlabMilestone gitlabMilestone, Iterable<MilestoneReleaseArtifactModel> gitlabArtifacts) throws IOException {
        Path artifactsPath = getArtifactsPath(gitlabMilestone);
        Files.createDirectories(artifactsPath.getParent());
        try (Writer reader = Files.newBufferedWriter(artifactsPath)) {
            gson.toJson(gitlabArtifacts, reader);
        }
    }

    public void setMilestoneExtraArtifacts(GitlabMilestone gitlabMilestone, Iterable<MilestoneReleaseArtifactModel> gitlabArtifacts) throws IOException {
        Path artifactsPath = getArtifactsExtraPath(gitlabMilestone);
        Files.createDirectories(artifactsPath.getParent());
        try (Writer reader = Files.newBufferedWriter(artifactsPath)) {
            gson.toJson(gitlabArtifacts, reader);
        }
    }

    private Path getProjectPath() {
        return cachePath.resolve("project.json");
    }

    private Path getMilestonePath(String gitlabMilestone) {
        return cachePath.resolve("milestones").resolve(gitlabMilestone + ".json");
    }

    private Path getMilestonePath(GitlabMilestone gitlabMilestone) {
        return cachePath.resolve("milestones").resolve(gitlabMilestone.getTitle() + ".json");
    }

    private Path getIssuesPath(GitlabMilestone gitlabMilestone) {
        return cachePath.resolve("milestones").resolve(gitlabMilestone.getTitle() + "-issues.json");
    }

    private Path getArtifactsPath(GitlabMilestone gitlabMilestone) {
        return cachePath.resolve("milestones").resolve(gitlabMilestone.getTitle() + "-artifacts.json");
    }

    private Path getArtifactsExtraPath(GitlabMilestone gitlabMilestone) {
        return cacheExtraPath.resolve("milestones").resolve(gitlabMilestone.getTitle() + "-artifacts.json");
    }

    private Set<Path> getPaths(Path path) throws IOException {
        Set<Path> paths = new LinkedHashSet<>();
        Files.walkFileTree(path, new SimpleFileVisitor<Path>() {

            @Override
            public FileVisitResult visitFile(Path file, BasicFileAttributes attrs) {
                if (file.toFile().getName().endsWith(".json")) {
                    paths.add(file);
                }
                return FileVisitResult.CONTINUE;
            }
        });
        return paths;
    }

}
