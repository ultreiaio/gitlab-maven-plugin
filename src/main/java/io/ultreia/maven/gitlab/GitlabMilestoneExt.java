package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.java4all.util.Version;
import org.gitlab.api.models.GitlabMilestone;

/**
 * Created by tchemit on 12/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GitlabMilestoneExt extends GitlabMilestone implements Comparable<GitlabMilestoneExt> {

    public static final String URL = "/-" + GitlabMilestone.URL;

    transient private Version version;

    @Override
    public int compareTo(GitlabMilestoneExt o) {
        return getVersion().compareTo(o.getVersion());
    }

    public Version getVersion() {
        if (version == null) {
            version = Version.valueOf(getTitle());
        }
        return version;
    }
}
