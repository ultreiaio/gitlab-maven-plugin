package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.commons.lang3.StringUtils;

import java.net.MalformedURLException;
import java.net.URL;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ReleaseArtifactModel {

    private String groupId;
    private String artifactId;
    private String type;
    private String classifier;
    private String name;
    private boolean flatLayout;

    public boolean isFlatLayout() {
        return flatLayout;
    }

    public void setFlatLayout(boolean flatLayout) {
        this.flatLayout = flatLayout;
    }

    public String getGroupId() {
        return groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public String getType() {
        return type;
    }

    public String getClassifier() {
        return classifier;
    }

    public String getName() {
        return name;
    }

    public URL getUrl(String host, String version, boolean flatLayout) {
        try {
            if (flatLayout) {
                return new URL(String.format("%s/%s-%s%s.%s",
                                             host,
                                             artifactId,
                                             version,
                                             StringUtils.isEmpty(classifier) ? "" : "-" + classifier,
                                             type));
            }
            return new URL(String.format("%s/%s/%s/%s/%s-%s%s.%s",
                                         host,
                                         groupId.replaceAll("\\.", "/"),
                                         artifactId,
                                         version,
                                         artifactId,
                                         version,
                                         StringUtils.isEmpty(classifier) ? "" : "-" + classifier,
                                         type));
        } catch (MalformedURLException e) {
            throw new IllegalStateException(e);
        }
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public void setName(String name) {
        this.name = name;
    }
}
