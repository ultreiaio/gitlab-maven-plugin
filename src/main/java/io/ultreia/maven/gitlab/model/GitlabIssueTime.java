package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.concurrent.TimeUnit;

/**
 * Created by tchemit on 11/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class GitlabIssueTime {

    public static final String URL = "/issues/%s/time_stats";

    @JsonProperty("time_estimate")
    private int timeEstimate;

    @JsonProperty("total_time_spent")
    private int totalTimeSpent;

    @JsonProperty("human_time_estimate")
    private String humanTimeEstimate;

    @JsonProperty("human_total_time_spent")
    private String humanTotalTimeSpend;

    public int getTimeEstimate() {
        return timeEstimate;
    }

    public void setTimeEstimate(int timeEstimate) {
        this.timeEstimate = timeEstimate;
    }

    public int getTotalTimeSpent() {
        return totalTimeSpent;
    }

    public void setTotalTimeSpent(int totalTimeSpent) {
        this.totalTimeSpent = totalTimeSpent;
    }

    public String getHumanTimeEstimate() {
        return humanTimeEstimate;
    }

    public void setHumanTimeEstimate(String humanTimeEstimate) {
        this.humanTimeEstimate = humanTimeEstimate;
    }

    public String getHumanTotalTimeSpend() {
        return humanTotalTimeSpend;
    }

    public void setHumanTotalTimeSpend(String humanTotalTimeSpend) {
        this.humanTotalTimeSpend = humanTotalTimeSpend;
    }

    public void add(GitlabIssueTime issueTime, int hoursByDay) {
        long nbHours = TimeUnit.SECONDS.toHours(issueTime.getTimeEstimate());
        long nbDays = nbHours / 8;
        long totalHours = nbHours % 8 + hoursByDay * nbDays;
        timeEstimate += totalHours;
    }

    public void recomputeHumanValues() {
        humanTimeEstimate = convertTime(timeEstimate);
        humanTotalTimeSpend = convertTime(totalTimeSpent);
    }

    private String convertTime(long timeEstimate) {
        return String.format("%dd %dh", timeEstimate / 6, timeEstimate % 6);
    }


    public void addEstimateTime(int pm) {
        timeEstimate += pm;
    }
}
