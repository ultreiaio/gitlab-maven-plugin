package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import org.gitlab.api.models.GitlabMilestone;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class MilestoneModel {

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd");

    private final GitlabMilestone milestone;
    private final List<IssueModel> issues;
    private final List<MilestoneReleaseArtifactModel> artifacts;

    public MilestoneModel(GitlabMilestone milestone, Set<IssueModel> issues, List<MilestoneReleaseArtifactModel> artifacts) {
        this.milestone = milestone;
        this.issues = new ArrayList<>(issues);
        this.artifacts = artifacts;
    }

    public boolean isWithIssue() {
        return !issues.isEmpty();
    }

    public boolean isWithNoIssue() {
        return issues.isEmpty();
    }

    public boolean isWithArtifact() {
        return !artifacts.isEmpty();
    }

    public List<MilestoneReleaseArtifactModel> getArtifacts() {
        return artifacts;
    }

    public List<IssueModel> getIssues() {
        return issues;
    }

    public int getId() {
        return milestone.getIid();
    }

    public String getTitle() {
        return milestone.getTitle();
    }

    public String getDescription() {
        return milestone.getDescription();
    }

    public String getDueDate() {
        return Optional.ofNullable(milestone.getDueDate()).map(SIMPLE_DATE_FORMAT::format).orElse("*In progress*");
    }

    public String getState() {
        return milestone.getState();
    }
}
