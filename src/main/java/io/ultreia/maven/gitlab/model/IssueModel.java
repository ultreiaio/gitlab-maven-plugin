package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import java.util.List;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabUser;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class IssueModel {

    private final GitlabIssue issue;
    private final GitlabIssueTime time;
    private final String tracker;
    private final String issueType;

    public IssueModel(GitlabIssue issue, GitlabIssueTime time, String tracker, List<String> trackers) {
        this.issue = issue;
        this.time = time;
        String issueType = "unknown";
        int i = trackers.indexOf(tracker);
        switch (i) {
            case 0:
                issueType = "fix";
                break;
            case 1:
                issueType = "add";
                break;
            case 2:
                issueType = "update";
                break;
            case 3:
                issueType = "remove";
                break;
        }
        this.tracker = tracker;
        this.issueType = issueType;
    }

    public IssueModel(GitlabIssue issue, String tracker, List<String> trackers) {
        this(issue, null, tracker, trackers);
    }

    public String getTracker() {
        return tracker;
    }

    public String getIssueType() {
        return issueType;
    }

    public int getId() {
        return issue.getIid();
    }

    public String getTitle() {
        return issue.getTitle();
    }

    public GitlabUser getAssignee() {
        return issue.getAssignee();
    }

    public GitlabUser getAuthor() {
        return issue.getAuthor();
    }

    public int getTimeEstimate() {
        return time.getTimeEstimate();
    }

    public int getTotalTimeSpent() {
        return time.getTotalTimeSpent();
    }

    public String getHumanTimeEstimate() {
        return time.getHumanTimeEstimate();
    }

    public String getHumanTotalTimeSpend() {
        return time.getHumanTotalTimeSpend();
    }
}
