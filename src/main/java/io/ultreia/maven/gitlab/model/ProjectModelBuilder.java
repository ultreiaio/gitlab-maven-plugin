package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.maven.gitlab.GitlabCache;
import io.ultreia.maven.gitlab.GitlabMilestoneExt;
import io.ultreia.maven.gitlab.GitlabMojoSupport;
import io.ultreia.maven.gitlab.ProjectNotFoundException;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.logging.Log;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabMilestone;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * To build a project model.
 * <p>
 * Created by tchemit on 21/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProjectModelBuilder {

    private final GitlabCache gitlabCache;
    private final Log log;
    private String milestone;
    private String title;
    private String author;
    private String authorEmail;
    private List<String> trackers;
    private String organizationId;
    private String projectId;
    private String host;
    private boolean verbose;

    public static ProjectModelBuilder create(GitlabMojoSupport mojo) {
        return new ProjectModelBuilder(mojo.newCache(), mojo.getLog())
                .setHost(mojo.getHost())
                .setVerbose(mojo.isVerbose())
                .setOrganizationId(mojo.getOrganizationId())
                .setProjectId(mojo.getProjectId());
    }

    private ProjectModelBuilder(GitlabCache gitlabCache, Log log) {
        this.gitlabCache = gitlabCache;
        this.log = log;
    }

    public ProjectModelBuilder setVerbose(boolean verbose) {
        this.verbose = verbose;
        return this;
    }

    public ProjectModelBuilder setHost(String host) {
        this.host = host;
        return this;
    }

    public ProjectModelBuilder setMilestone(String milestone) {
        if (milestone != null && milestone.endsWith("-SNAPSHOT")) {
            milestone = StringUtils.removeEnd(milestone, "-SNAPSHOT");
        }
        this.milestone = milestone;
        return this;
    }

    public ProjectModelBuilder setTitle(String title) {
        this.title = title;
        return this;
    }

    public ProjectModelBuilder setAuthor(String author) {
        this.author = author;
        return this;
    }

    public ProjectModelBuilder setAuthorEmail(String authorEmail) {
        this.authorEmail = authorEmail;
        return this;
    }

    public ProjectModelBuilder setTrackers(List<String> trackers) {
        this.trackers = trackers;
        return this;
    }

    public ProjectModelBuilder setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public ProjectModelBuilder setProjectId(String projectId) {
        this.projectId = projectId;
        return this;
    }

    public ProjectModel build() throws IOException, ProjectNotFoundException {
        Objects.requireNonNull(gitlabCache);
        Objects.requireNonNull(host);
        Objects.requireNonNull(organizationId);
        Objects.requireNonNull(projectId);
        Objects.requireNonNull(title);
        Objects.requireNonNull(author);
        Objects.requireNonNull(authorEmail);
        Objects.requireNonNull(trackers);

        if (verbose) {
            log.info("loading project...");
        }

        Optional<GitlabProject> gitlabProjectOptional = gitlabCache.getProject();
        if (!gitlabProjectOptional.isPresent()) {
            throw new ProjectNotFoundException();
        }
        String projectPath = organizationId + "/" + this.projectId;
        GitlabProject gitlabProject = gitlabProjectOptional.get();
        log.info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));

        if (verbose) {
            log.info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = gitlabCache.getMilestones();

        log.info(String.format("found %d milestone(s).", gitlabMilestones.size()));

        Set<MilestoneModel> milestoneModels = new LinkedHashSet<>();
        for (GitlabMilestone gitlabMilestone : gitlabMilestones) {

            String gitlabMilestoneTitle = gitlabMilestone.getTitle();
            boolean onCurrentMilestone = milestone != null && milestone.equals(gitlabMilestoneTitle);
            if (!"closed".equals(gitlabMilestone.getState()) && !onCurrentMilestone) {
                log.info(String.format("Skip open milestone %s.", gitlabMilestoneTitle));
                continue;
            }
            log.info(String.format("GitLab milestone (%s) id: %d", gitlabMilestoneTitle, gitlabMilestone.getIid()));

            List<MilestoneReleaseArtifactModel> milestoneReleaseArtifactModels = new LinkedList<>();
            if (verbose) {
                log.info("loading artifacts...");
            }
            Set<MilestoneReleaseArtifactModel> milestoneArtifacts = gitlabCache.getMilestoneArtifacts(gitlabMilestone);
            if (milestoneArtifacts != null && !milestoneArtifacts.isEmpty()) {
                milestoneReleaseArtifactModels.addAll(milestoneArtifacts);
            }
            if (verbose) {
                log.info(String.format("found %d artifact(s).", milestoneReleaseArtifactModels.size()));
            }
            if (verbose) {
                log.info("loading issues...");
            }
            Set<GitlabIssue> gitlabIssues = gitlabCache.getMilestoneIssues(gitlabMilestone);
            if (verbose) {
                log.info(String.format("found %d issue(s).", gitlabIssues.size()));
            }
            Set<IssueModel> issues = new LinkedHashSet<>();
            for (String tracker : trackers) {
                List<IssueModel> issueModels = gitlabIssues.stream()
                        .filter(i -> Arrays.asList(i.getLabels()).contains(tracker))
                        .map(i -> new IssueModel(i, tracker, trackers))
                        .sorted(Comparator.comparing(IssueModel::getId))
                        .collect(Collectors.toList());
                issues.addAll(issueModels);
            }
            milestoneModels.add(new MilestoneModel(gitlabMilestone, issues, milestoneReleaseArtifactModels));

        }

        return new ProjectModel(host + "/" + projectPath,
                                title, author, authorEmail, milestoneModels, trackers);

    }
}
