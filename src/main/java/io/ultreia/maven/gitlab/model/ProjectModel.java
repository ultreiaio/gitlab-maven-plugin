package io.ultreia.maven.gitlab.model;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.maven.gitlab.GitlabIssueExt;
import io.ultreia.maven.gitlab.GitlabMilestoneExt;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Set;

/**
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
public class ProjectModel {

    private final String issueUrl;
    private final String milestoneUrl;
    private final String title;
    private final String author;
    private final String authorEmail;

    private final List<String> trackers;

    private final List<MilestoneModel> milestones;

    public ProjectModel(String projectUrl, String title, String author, String authorEmail, Set<MilestoneModel> milestones, List<String> trackers) {
        this.milestoneUrl = projectUrl + GitlabMilestoneExt.URL;
        this.issueUrl = projectUrl + GitlabIssueExt.URL;
        this.title = title;
        this.author = author;
        this.authorEmail = authorEmail;
        this.trackers = trackers;
        this.milestones = new ArrayList<>(milestones);
        Collections.reverse(this.milestones);
    }

    private static final SimpleDateFormat SIMPLE_DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm");

    public String getNow() {
        return SIMPLE_DATE_FORMAT.format(new Date());
    }

    public String getMilestoneUrl() {
        return milestoneUrl;
    }

    public String getIssueUrl() {
        return issueUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getAuthor() {
        return author;
    }

    public String getAuthorEmail() {
        return authorEmail;
    }

    public List<String> getTrackers() {
        return trackers;
    }

    public List<MilestoneModel> getMilestones() {
        return milestones;
    }
}
