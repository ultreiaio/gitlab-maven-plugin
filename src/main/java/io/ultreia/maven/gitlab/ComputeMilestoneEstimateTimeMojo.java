package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import com.github.mustachejava.DefaultMustacheFactory;
import com.github.mustachejava.Mustache;
import com.github.mustachejava.MustacheFactory;
import io.ultreia.maven.gitlab.model.GitlabIssueTime;
import io.ultreia.maven.gitlab.model.IssueModel;
import io.ultreia.maven.gitlab.model.MilestoneModel;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabProject;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.Writer;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

/**
 * Compute the estimate time of a given milestone and generate the report in a file.
 * <p>
 * Created by tchemit on 11/04/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "compute-milestone-estimate-time", defaultPhase = LifecyclePhase.INITIALIZE)
public class ComputeMilestoneEstimateTimeMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    /**
     * The first milestone to use.
     */
    @Parameter(property = "gitlab.fromMilestone")
    private String fromMilestone;

    /**
     * Comma separated tracker names (they are gitlab label) and they match the type of issues used by the changes.xml
     * format {@code fix, add, update, remove} in this precise order.
     * <p>
     * <strong>Note:</strong> order matter
     */
    @Parameter(property = "gitlab.trackers", required = true)
    private String trackers;

    /**
     * Where to generate report.
     */
    @Parameter(property = "gitlab.output", required = true, defaultValue = "${project.build.directory}/milestone-estimate-time.md")
    private File output;

    /**
     * Template to use to generate report, you can use an external file.
     * Available model in mustache template is {@link ExportModel}.
     */
    @Parameter(property = "gitlab.template", required = true, defaultValue = "classpath://compute-estimate-time.md.mustache")
    private String template;

    /**
     * Number of hours we count in a day.
     * Note that gitlab use 8 hours per day.
     */
    @Parameter(property = "gitlab.hoursByDay", required = true, defaultValue = "6")
    private int hoursByDay;

    /**
     * Ratio to use to get management cost from the total estimate time of issues
     */
    @Parameter(property = "gitlab.managementRatio", required = true, defaultValue = "0.25")
    private float managementRatio;

    /**
     * To skip goal.
     */
    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException, ProjectNotFoundException {
        milestone = removeSnapShot(milestone);
        if (isVerbose()) {
            getLog().info("loading project...");
        }
        String projectPath = getGitlabProjectPath();
        GitlabProject gitlabProject = api.getProject(projectPath);

        GitlabCache gitlabCache = newCache();
        gitlabCache.setProject(gitlabProject);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        if (isVerbose()) {
            getLog().info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = api.getMilestones(gitlabProject, fromMilestone);
        getLog().info(String.format("found %d milestone(s).", gitlabMilestones.size()));

        Optional<GitlabMilestoneExt> optionalMilestone = gitlabMilestones.stream().filter(m -> milestone.equals(m.getTitle())).findFirst();
        if (!optionalMilestone.isPresent()) {
            throw new MilestoneNotFoundException(milestone);
        }

        GitlabMilestoneExt gitlabMilestone = optionalMilestone.get();
        getLog().info(String.format("found milestone %s", gitlabMilestone.getTitle()));

        if (isVerbose()) {
            getLog().info("loading issues...");
        }
        List<GitlabIssue> gitlabIssues = api.getMilestoneIssues(gitlabProject, gitlabMilestone);
        getLog().info(String.format("found %d issue(s).", gitlabIssues.size()));

        Mustache mustache;
        if (template.startsWith("classpath://")) {
            String file = template.substring("classpath://".length());
            mustache = getMustache(file);
        } else {
            MustacheFactory mf = new DefaultMustacheFactory();
            try (BufferedReader bufferedReader = Files.newBufferedReader(Paths.get(template))) {
                mustache = mf.compile(bufferedReader, "export-milestone");
            }
        }

        List<String> trackersList = Arrays.asList(trackers.split("\\s*,\\s*"));
        Set<IssueModel> issues = new LinkedHashSet<>();
        GitlabIssueTime issuesTotalTime = new GitlabIssueTime();

        Map<String, GitlabIssueTime> totalByTrackers = new LinkedHashMap<>();
        for (String tracker : trackersList) {
            List<IssueModel> issueModels = new LinkedList<>();

            GitlabIssueTime trackerTotalTime = new GitlabIssueTime();
            gitlabIssues.stream()
                    .filter(i -> Arrays.asList(i.getLabels()).contains(tracker))
                    .forEach(i -> {
                        try {
                            GitlabIssueTime issueTime = api.getIssueTime(gitlabProject, i.getId());
                            if (issueTime.getTimeEstimate() > 0) {
                                trackerTotalTime.add(issueTime, hoursByDay);
                                IssueModel issueModel = new IssueModel(i, issueTime, tracker, trackersList);
                                issueModels.add(issueModel);
                            }
                        } catch (IOException e) {
                            throw new RuntimeException("Can't load times for issue: " + i.getTitle(), e);
                        }

                    });
            issueModels.sort(Comparator.comparing(IssueModel::getId));
            issues.addAll(issueModels);
            if (!issueModels.isEmpty()) {
                trackerTotalTime.recomputeHumanValues();
                totalByTrackers.put(tracker, trackerTotalTime);
                issuesTotalTime.addEstimateTime(trackerTotalTime.getTimeEstimate());
            }
        }

        issuesTotalTime.recomputeHumanValues();
        totalByTrackers.put("Issues total", issuesTotalTime);

        GitlabIssueTime projectManagmentTime = new GitlabIssueTime();
        int timeEstimate = issuesTotalTime.getTimeEstimate();
        float pm = timeEstimate * managementRatio;
        projectManagmentTime.addEstimateTime((int) pm);
        projectManagmentTime.recomputeHumanValues();
        totalByTrackers.put("Project managment", projectManagmentTime);

        GitlabIssueTime totalTime = new GitlabIssueTime();
        totalTime.addEstimateTime(issuesTotalTime.getTimeEstimate());
        totalTime.addEstimateTime(projectManagmentTime.getTimeEstimate());
        totalTime.recomputeHumanValues();

        totalByTrackers.put("Total  ", totalTime);
        ExportModel exportModel = new ExportModel(new MilestoneModel(gitlabMilestone, issues, null), totalByTrackers);

        try (Writer writer = Files.newBufferedWriter(output.toPath(), StandardCharsets.UTF_8)) {
            mustache.execute(writer, exportModel).flush();
        }

        getLog().info(String.format("Milestone issues exported to to %s", output));

    }

    class ExportModel {

        private final MilestoneModel milestoneModel;
        private Map<String, GitlabIssueTime> totalTimeByTrackers;

        ExportModel(MilestoneModel milestoneModel, Map<String, GitlabIssueTime> totalTimeByTrackers) {
            this.milestoneModel = milestoneModel;
            this.totalTimeByTrackers = totalTimeByTrackers;
        }

        public MilestoneModel getMilestoneModel() {
            return milestoneModel;
        }

        public Set<Map.Entry<String, GitlabIssueTime>> getTotalTimeByTrackers() {
            return totalTimeByTrackers.entrySet();
        }

        public List<IssueModel> getIssues() {
            return milestoneModel.getIssues();
        }
    }
}

