package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.List;
import java.util.Optional;

/**
 * Check if a given milestone exists and fail build if the milestone was not found.
 * <p>
 * Created by tchemit on 12/03/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "check-milestone-exist", defaultPhase = LifecyclePhase.INITIALIZE)
public class CheckMilestoneExistMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    /**
     * The first milestone to use.
     */
    @Parameter(property = "gitlab.fromMilestone")
    private String fromMilestone;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException, ProjectNotFoundException {
        milestone = removeSnapShot(milestone);
        if (isVerbose()) {
            getLog().info("loading project...");
        }
        String projectPath = getGitlabProjectPath();
        GitlabProject gitlabProject = api.getProject(projectPath);

        GitlabCache gitlabCache = newCache();
        gitlabCache.setProject(gitlabProject);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        if (isVerbose()) {
            getLog().info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = api.getMilestones(gitlabProject, fromMilestone);
        getLog().info(String.format("found %d milestone(s).", gitlabMilestones.size()));

        Optional<GitlabMilestoneExt> optionalMilestone = gitlabMilestones.stream().filter(m -> milestone.equals(m.getTitle())).findFirst();
        if (!optionalMilestone.isPresent()) {
            throw new MilestoneNotFoundException(milestone);
        }
        getLog().info(String.format("found milestone %s.", optionalMilestone.get().getTitle()));
    }
}
