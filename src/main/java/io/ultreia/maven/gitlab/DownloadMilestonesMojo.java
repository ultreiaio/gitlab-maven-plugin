package io.ultreia.maven.gitlab;

/*-
 * #%L
 * Gitlab Maven Plugin
 * %%
 * Copyright (C) 2017 - 2021 Ultreia.io
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * #L%
 */

import io.ultreia.maven.gitlab.model.MilestoneReleaseArtifactModel;
import io.ultreia.maven.gitlab.model.ReleaseArtifactModel;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.gitlab.api.models.GitlabIssue;
import org.gitlab.api.models.GitlabProject;

import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;

/**
 * Download all milestones and their issues of a GitLab project to a local directory cache.
 * <p>
 * Created by tchemit on 20/02/17.
 *
 * @author Tony Chemit - dev@tchemit.fr
 */
@Mojo(name = "download-milestones", defaultPhase = LifecyclePhase.GENERATE_RESOURCES)
public class DownloadMilestonesMojo extends GitlabMojoSupport {

    @Parameter(property = "gitlab.milestone", required = true, defaultValue = "${project.version}")
    private String milestone;

    /**
     * The first milestone to use.
     */
    @Parameter(property = "gitlab.fromMilestone")
    private String fromMilestone;

    @Parameter(property = "gitlab.force")
    private boolean force;

    @Parameter(property = "gitlab.forceArtifacts")
    private boolean forceArtifacts;

    @Parameter(property = "gitlab.skip")
    private boolean skip;

    @Parameter(property = "gitlab.forceForCurrentMilestone", defaultValue = "true")
    private boolean forceForCurrentMilestone;

    @Parameter
    private List<ReleaseArtifactModel> releaseArtifacts;

    @Override
    boolean isSkip() {
        return skip || shouldSkip();
    }

    @Override
    protected void execute(GitlabAPIExt api) throws IOException, MilestoneNotFoundException {

        milestone = removeSnapShot(milestone);

        if (isVerbose()) {
            getLog().info("loading project...");
        }
        String projectPath = getGitlabProjectPath();
        GitlabProject gitlabProject = api.getProject(projectPath);

        GitlabCache gitlabCache = newCache();
        gitlabCache.setProject(gitlabProject);

        getLog().info(String.format("GitLab project (%s) id: %d", projectPath, gitlabProject.getId()));
        if (isVerbose()) {
            getLog().info("loading milestones...");
        }
        List<GitlabMilestoneExt> gitlabMilestones = api.getMilestones(gitlabProject, fromMilestone);
        int gitlabMilestonesCount = gitlabMilestones.size();
        getLog().info(String.format("found %d milestone(s).", gitlabMilestonesCount));

        Optional<GitlabMilestoneExt> optionalMilestone = gitlabMilestones.stream().filter(m -> milestone.equals(m.getTitle())).findFirst();

        int index = 0;
        for (GitlabMilestoneExt gitlabMilestone : gitlabMilestones) {

            if (isVerbose()) {
                getLog().info(String.format("[%1$-4d/%2$-4d] Process GitLab milestone (%3$s) id: %4$d", ++index, gitlabMilestonesCount, gitlabMilestone.getTitle(), gitlabMilestone.getId()));
            }
            Optional<GitlabMilestoneExt> milestoneOptional = gitlabCache.getMilestone(gitlabMilestone);

            if (milestoneOptional.isPresent()) {

                if (force) {
                    storeMilestone(api, gitlabProject, gitlabCache, gitlabMilestone);
                    continue;
                }

                if (forceForCurrentMilestone && optionalMilestone.isPresent() && gitlabMilestone.getId() == optionalMilestone.get().getId()) {
                    storeMilestone(api, gitlabProject, gitlabCache, gitlabMilestone);
                    continue;
                }

                if (storeMilestoneArtifacts(gitlabCache, gitlabMilestone)) {
                    continue;
                }

                if (isVerbose()) {
                    getLog().info("Skip existing milestone");
                }
                continue;
            }

            storeMilestone(api, gitlabProject, gitlabCache, gitlabMilestone);
        }

    }

    private void storeMilestone(GitlabAPIExt api, GitlabProject gitlabProject, GitlabCache gitlabCache, GitlabMilestoneExt gitlabMilestone) throws IOException {
        gitlabCache.setMilestone(gitlabMilestone);
        if (isVerbose()) {
            getLog().info("loading issues...");
        }
        List<GitlabIssue> issues = api.getMilestoneIssues(gitlabProject, gitlabMilestone);
        if (isVerbose()) {
            getLog().info(String.format("found %d issue(s).", issues.size()));
        }
        gitlabCache.setMilestoneIssues(gitlabMilestone, issues);
        storeMilestoneArtifacts(gitlabCache, gitlabMilestone);
    }

    private boolean storeMilestoneArtifacts(GitlabCache gitlabCache, GitlabMilestoneExt gitlabMilestone) throws IOException {
        if (releaseArtifacts == null) {
            return false;
        }
        if (isVerbose()) {
            getLog().info("loading artifacts...");
        }
        Set<MilestoneReleaseArtifactModel> milestoneArtifacts = gitlabCache.getMilestoneArtifacts(gitlabMilestone);
        if (milestoneArtifacts != null && !milestoneArtifacts.isEmpty() && !forceArtifacts) {
            return false;
        }
        milestoneArtifacts = loadArtifacts(releaseArtifacts, gitlabMilestone.getTitle());
        if (isVerbose()) {
            getLog().info(String.format("found %d artifact(s).", milestoneArtifacts.size()));
        }
        gitlabCache.setMilestoneArtifacts(gitlabMilestone, milestoneArtifacts);
        return true;
    }

}
