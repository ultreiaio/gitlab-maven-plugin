[![Maven Central status](https://img.shields.io/maven-central/v/io.ultreia.maven/gitlab-maven-plugin.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22io.ultreia.maven%22%20AND%20a%3A%22gitlab-maven-plugin%22)
[![build status](https://gitlab.com/ultreiaio/gitlab-maven-plugin/badges/develop/pipeline.svg)](https://gitlab.com/ultreiaio/gitlab-maven-plugin/commits/develop)
[![The GNU Lesser General Public License, Version 3.0](https://img.shields.io/badge/license-LGPL3-grren.svg)](http://www.gnu.org/licenses/lgpl-3.0.txt)

# Gitlab Maven Plugin

Goals:

 * download-milestones
 * generate-changes
 * generate-changelog
 * generate-staging-changelog
 * check-milestone-exist
 * create-milestone
 * close-milestone
 * compute-milestone-estimate-time
 
[Documentation](https://ultreiaio.gitlab.io/gitlab-maven-plugin) will come soon. Be patient a bit. 